'use strict'; //guards us from not declared variables etc.

document.addEventListener('DOMContentLoaded', buildMenu, false);
 
function buildMenu() {
    let menu = document.createElement('div');
    let content = document.createElement('div');
    menu.id = 'menu';
    content.id = 'content';
    content.innerHTML = document.body.innerHTML;
    document.body.innerHTML = '';
    document.body.appendChild(menu);
    document.body.appendChild(content);

    let menuList = document.createElement('ol');
    menu.appendChild(menuList);
    menuList = traverseContentAndGenerateMenu(content, menuList);
}

function traverseContentAndGenerateMenu(content, menuList) {
    let header = content.firstElementChild;
    let lastLevel = 1;
    do {
        if (header.tagName.toLowerCase().startsWith('h')) {
            let currentLevel = Number(header.tagName[1]);
            if (currentLevel > lastLevel) {
                menuList = nestElementsOnTheBasisOfCurrentLevel(currentLevel, lastLevel, menuList);
            } else if (currentLevel < lastLevel) {
                menuList = getBackToLastLevelInMenu(lastLevel, currentLevel, menuList);
            }

            const headerText = header.innerHTML;
            let idFromHeader = generateIdFromText(headerText);
            header.id = idFromHeader;
            generateMenuItem(headerText, idFromHeader, menuList);

            lastLevel = currentLevel;
        }
        header = header.nextElementSibling;
    } while (header);
    return menuList;
}

function nestElementsOnTheBasisOfCurrentLevel(currentLevel, lastLevel, menuList) {
    let levelDiff = currentLevel - lastLevel;
    while (levelDiff--) {
        let tempMenuList = document.createElement('ol');
        menuList.appendChild(tempMenuList);
        menuList = tempMenuList;
    }
    return menuList;
}

function getBackToLastLevelInMenu(lastLevel, currentLevel, menuList) {
    let levelDiff = lastLevel - currentLevel;
    while (levelDiff--) {
        menuList = menuList.parentElement;
    }
    return menuList;
}

function generateMenuItem(headerText, headerId, menuList) {
    let menuItem = document.createTextNode(headerText);
    let menuItemLi = document.createElement('li');
    let menuItemA = document.createElement('a');
    menuItemA.href = '#' + headerId;
    menuItemA.appendChild(menuItem);
    menuItemLi.appendChild(menuItemA);
    menuList.appendChild(menuItemLi);
}

function generateIdFromText(headerText) {
    let nonCharRegEx = /[^a-z]/g;
    let idedValue = headerText.toLowerCase().replace(nonCharRegEx, '-');
    return idedValue;
}
