function drawX(event) {
    var x = event.clientX - this.getBoundingClientRect().left;
    var y = event.clientY - this.getBoundingClientRect().top;
    var width = this.getBoundingClientRect().width;
    var height = this.getBoundingClientRect().height;
    var safeDistance = Math.max(width,height);

    var ctx = this.getContext('2d');
    ctx.clearRect(0,0,width,height);
    ctx.beginPath();
    ctx.moveTo(x-safeDistance,y-safeDistance);
    ctx.lineTo(x+safeDistance,y+safeDistance);
    ctx.strokeStyle = 'blue';
    ctx.lineWidth = 2;
    ctx.stroke();
    ctx.beginPath();
    ctx.moveTo(x+safeDistance,y-safeDistance);
    ctx.lineTo(x-safeDistance,y+safeDistance);
    ctx.strokeStyle = 'green';
    ctx.lineWidth = 4;
    ctx.stroke();
}

function initializeApplication() {
    var canvas = document
        .getElementById("canvas-element");
    canvas.addEventListener("mousemove", drawX);
}

document.addEventListener("DOMContentLoaded", initializeApplication);