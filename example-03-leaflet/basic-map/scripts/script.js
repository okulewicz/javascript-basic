`use strict;`

const mapContainerId = 'map-panel';
let map;

document.addEventListener('DOMContentLoaded', initializeApplication, false);

function initializeApplication() {
    loadMap();
}

function loadMap() {
    //add map and set its center to specific location
    map = L.map(mapContainerId).setView([52.05, 20.00], 6);
    //add OSM as background layer
    let osmLayer = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png ', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">' +
            'OpenStreetMap</a> contributors, ' +
            '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
        maxZoom: 19,
        id: 'osm.tiles'
    });
    osmLayer.addTo(map);
}