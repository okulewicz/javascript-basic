`use strict;`

const mapContainerId = 'map-panel';
let map;

document.addEventListener('DOMContentLoaded', initializeApplication, false);

function initializeApplication() {
    loadMap();
}

function loadMap() {
    //add map and set its center to specific location
    map = L.map(mapContainerId).setView([52.05, 20.00], 6);
    //add OSM as background layer
    let osmLayer = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png ', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">' +
            'OpenStreetMap</a> contributors, ' +
            '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
        maxZoom: 19,
        id: 'osm.tiles'
    });
    osmLayer.addTo(map);

    var gdosWMSNatura2000Ock = L.tileLayer.wms("http://sdi.gdos.gov.pl/wms", {
        layers: ['GDOS:ObszaryChronionegoKrajobrazu'],
        format: 'image/png',
        transparent: true,
        attribution: "GDOS"
    });
    var gdosWMSNatura2000Oso = L.tileLayer.wms("http://sdi.gdos.gov.pl/wms", {
        layers: ['GDOS:ObszarySpecjalnejOchrony'],
        format: 'image/png',
        transparent: true,
        attribution: "GDOS"
    });
    var gdosWMSNationalParks = L.tileLayer.wms("https://sdi.gdos.gov.pl/wms", {
        layers: ['GDOS:ParkiNarodowe'],
        format: 'image/png',
        transparent: true,
        attribution: "GDOS"
    });
    var gdosWMSLandscapeParks = L.tileLayer.wms("https://sdi.gdos.gov.pl/wms", {
        layers: ['GDOS:ParkiKrajobrazowe'],
        format: 'image/png',
        transparent: true,
        attribution: "GDOS"
    });

    var gdosWMSLayerGroup = L.layerGroup()
        .addLayer(gdosWMSNatura2000Ock)
        .addLayer(gdosWMSNatura2000Oso)
        .addLayer(gdosWMSNationalParks)
        .addLayer(gdosWMSLandscapeParks);
    gdosWMSLayerGroup.addTo(map);

    gdosWMSLayerGroup.addTo(map);

}