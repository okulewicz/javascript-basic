`use strict;`

const mapContainerId = 'map-panel';
let map;

document.addEventListener('DOMContentLoaded', initializeApplication, false);

function initializeApplication() {
    loadMap();
}

function loadMap() {
    //add map and set its center to specific location
    map = L.map(mapContainerId).setView([52.05, 20.00], 6);
    //add OSM as background layer
    let osmLayer = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png ', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">' +
            'OpenStreetMap</a> contributors, ' +
            '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
        maxZoom: 19,
        id: 'osm.tiles'
    });
    osmLayer.addTo(map);

    let marker1 = L.marker([52.2217988, 21.0071782]);
    let marker2 = L.marker([52.5617037, 19.6790427]);
    marker1.bindPopup('Warsaw University of Technology<br />Main Campus');
    marker2.bindPopup('Warsaw University of Technology<br />City of Płock Division');

    marker1.addTo(map);
    marker2.addTo(map);
}